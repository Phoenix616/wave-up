<!--
  ~ Copyright (c) 2016-2018 Juan García Basilio
  ~ Copyright (c) 2018 tau.gi.mo
  ~
  ~ This file is part of WaveUp.
  ~
  ~ WaveUp is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ WaveUp is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
  -->

<resources>
    <string name="app_name">WaveUp</string>

    <string name="prefs_service">Service</string>
    <string name="pref_enable">Einschalten</string>

    <string name="prefs_modes">WaveUp-Modi</string>
    <string name="pref_wave_mode">Winkmodus</string>
    <string name="pref_wave_mode_summary">Display einschalten, wenn man ein mal mit der Hand über den Proximity-Sensor winkt.    </string>
    <string name="pref_pocket_mode">Taschenmodus</string>
    <string name="pref_pocket_mode_summary">Display einschalten, wenn man das Gerät aus der Tasche holt.    </string>

    <string name="pref_lock_screen">Gerät sperren</string>
    <string name="pref_lock_screen_mode_summary">Display ausschalten und Gerät sperren, wenn der Proximity-Sensor bedeckt wird.</string>
    <string name="pref_lock_screen_when_landscape">Sperren in Landscape-Modus</string>
    <string name="pref_lock_screen_when_landscape_summary">Wenn deaktiviert, wird vermieden, dass das Gerät sich sperrt, während es horizontal gehalten wird, z.B. um Media zu schauen.</string>
    <string name="pref_lock_screen_with_power_button_as_root">Workaround für Fingerprint- und Smart-Lock</string>
    <string name="pref_lock_screen_with_power_button_as_root_summary">Simuliert den An/Aus-Knopf, um den Bildschirm auszuchalten. Benötigt root.</string>
    <string name="pref_lock_screen_vibrate_on_lock">Vorm Sperren vibrieren</string>
    <string name="pref_lock_screen_vibrate_on_lock_summary">Das Gerät vibriert, wenn der Proximity-Sensor bedeckt wird, um versehentliche Sperrungen zu vermeiden.</string>
    <string name="pref_sensor_cover_time_before_locking_screen">Deckzeit zum Sperren des Geräts</string>
    <string name="pref_sensor_cover_time_before_locking_screen_summary">Zeit der Proximity-Sensor bedeckt werden muss, damit WaveUp das Gerät sperrt. Aktueller Wert: %s.</string>

    <string name="wave_up_service_started">WaveUp-Service wird ausgeführt</string>
    <string name="wave_up_service_stopped">WaveUp-Service wird nicht ausgeführt</string>

    <string name="lock_admin_rights_explanation"><b>Gerät sperren: Geräte Administrator </b>\n\nUm den Bildschirm zu sperren, muss WaveUp Administratorrechte erhalten. \n\nSobald eine App Administratorrechte hat, kann man diese nicht einfach deinstallieren ohne die Administratorrechte zu widerrufen. Wenn du die Administratorrechte widerrufen möchtest, öffne einfach WaveUp, gehe zu den erweiterten Optionen (obere rechte Ecke) und tippe auf \"Widerrufe Adminberechtigung für dieses Gerät\". Alternativ gehe zu <i> Einstellungen -> Sicherheit -> Geräteadministratoren</i> und entferne den Haken bei \"WaveUp\". Wenn du WaveUp deinstallieren möchtest tippe einfach auf den \"WAVEUP DEINSTALLIEREN\"-Button unten in der App. Dies widerruft die Administratorrechte <i>und </i>deinstalliert die App sofort.\n\nBist du sicher dass du WaveUp Administratorrechte für dein Gerät erteilen möchtest?</string>
    <string name="root_access_failed">Root-Zugriff wurde nicht erlaubt</string>

    <string name="revoke_device_admin_permission_menu_item">Widerrufe Adminberechtigung für dieses Gerät</string>

    <string name="uninstall_button">WaveUp deinstallieren</string>
    <string name="removed_device_admin_rights">WaveUp ist kein Gerätadministrator mehr. Drücken Sie die "Gerät sperren" Option, um es wieder einzurichten</string>

    <!-- Initial alert dialog -->
    <string name="alert_dialog_title">Willkommen bei WaveUp!</string>
    <string name="alert_dialog_message"><b>Deinstallieren:</b> WaveUp muss von innerhalb der App deinstalliert werden (es gibt einen Knopf dafür ganz unten).\n\n<b>(Angeblicher) übertriebene Akkuverbrauch:</b> Einige Android-Versionen berichten einen (falschen) sehr hohen Akkuverbrauch.\n\nViel Spaß mit WaveUp! :)</string>
    <string name="alert_dialog_ok_button">Okay</string>

    <!-- I put this array here instead of in arrays.xml because it has to be translated. -->
    <string-array name="sensor_cover_time_entries">
        <item>Sofort</item>
        <item>0,5 Sekunden</item>
        <item>1 Sekunde</item>
        <item>1,5 Sekunden</item>
        <item>2 Sekunden</item>
        <item>5 Sekunden</item>
    </string-array>

    <string name="pref_number_of_waves">Winkzahl</string>
    <string name="pref_number_of_waves_summary">Anzahl der Winken (bedecken und aufdecken), um das Display einzuschalten. Aktueller Wert: %s.</string>

    <string-array name="number_of_waves_entries">
        <item>1 (bedecken, aufdecken)</item>
        <item>2 (bedecken, aufdecken, bedecken, aufdecken)</item>
        <item>3 (bedecken, aufdecken, bedecken, aufdecken, bedecken, aufdecken)</item>
    </string-array>

    <string name="privacy_policy_menu_item">Datenschutzerklärung</string>
    </resources>

/*
 * Copyright (c) 2016 Juan García Basilio
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.waveup;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.IBinder;

import com.jarsilio.android.waveup.receivers.CallStateReceiver;
import com.jarsilio.android.waveup.receivers.OrientationReceiver;
import com.jarsilio.android.waveup.receivers.ScreenReceiver;

import timber.log.Timber;

public class WaveUpService extends Service {
    private ProximitySensorManager proximitySensorManager;
    private OrientationReceiver orientationReceiver;
    private ScreenReceiver screenReceiver;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(orientationReceiver);
        unregisterReceiver(screenReceiver);
        unregisterCallStateReceiver();
        proximitySensorManager.stop();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        proximitySensorManager = ProximitySensorManager.getInstance(getApplicationContext());
        registerScreenReceiver();
        registerOrientationReceiver();
        registerCallStateReceiver();
    }

    private void registerScreenReceiver() {
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        if (screenReceiver == null) {
            screenReceiver = new ScreenReceiver();
        }
        registerReceiver(screenReceiver, filter);
    }

    private void registerOrientationReceiver() {
        IntentFilter filter = new IntentFilter(Intent.ACTION_CONFIGURATION_CHANGED);
        if (orientationReceiver == null) {
            orientationReceiver = new OrientationReceiver();
        }
        registerReceiver(orientationReceiver, filter);
    }

    private void registerCallStateReceiver() {
        Timber.d("Registering PHONE_STATE receiver (as component)");
        ComponentName component = new ComponentName(getApplicationContext(), CallStateReceiver.class);
        getPackageManager().setComponentEnabledSetting(component, PackageManager.COMPONENT_ENABLED_STATE_ENABLED , PackageManager.DONT_KILL_APP);
    }

    private void unregisterCallStateReceiver() {
        /*
         * For some reason, I actually don't need to actively register this one. It is enough for it
         * to be declared in the manifest. However, I cannot call 'unregisterReceiver' like for the
         * other receivers but I still want to stop receving the phone intents if the service is
         * deactivated.
         *
         * My source of inspiration: https://stackoverflow.com/questions/6529276/android-how-to-unregister-a-receiver-created-in-the-manifest
         */

        Timber.d("Unregistering PHONE_STATE receiver (as component)");
        ComponentName component = new ComponentName(getApplicationContext(), CallStateReceiver.class);
        getPackageManager().setComponentEnabledSetting(component, PackageManager.COMPONENT_ENABLED_STATE_DISABLED , PackageManager.DONT_KILL_APP);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        proximitySensorManager.startOrStopListeningDependingOnConditions();
        return super.onStartCommand(intent, flags, startId);
    }
}

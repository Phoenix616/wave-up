/*
 * Copyright (c) 2016-2017 Juan García Basilio
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.waveup;

import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.os.PowerManager;
import android.os.Vibrator;

import timber.log.Timber;

public class ScreenHandler {
    private static final long TIME_SCREEN_ON = 5000;

    private final PowerManager powerManager;
    private final PowerManager.WakeLock wakeLock;
    private final DevicePolicyManager policyManager;
    private final WaveUpWorldState waveUpWorldState;

    private final Settings settings;

    private long lastTimeScreenOnOrOff;

    private final Context context;

    private static volatile ScreenHandler instance;

    private Thread turnOffScreenThread;
    private boolean turningOffScreen;

    public static ScreenHandler getInstance(Context context) {
        if (instance == null ) {
            synchronized (ScreenHandler.class) {
                if (instance == null) {
                    instance = new ScreenHandler(context);
                }
            }
        }

        return instance;
    }

    private ScreenHandler(Context context) {
        this.context = context;
        this.powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        this.wakeLock = powerManager.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "WakeUpWakeLock");
        this.policyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        this.settings = Settings.getInstance(context);
        this.waveUpWorldState = new WaveUpWorldState(context);
    }

    private Thread turnOffScreenThread(final long delay) {
        return new Thread() {
            @Override
            public void run() {
                if (waveUpWorldState.isScreenOn()) {
                    Timber.d("Creating a thread to turn off display if still covered in " + delay/1000 + " seconds");
                    try {
                        Thread.sleep(delay);
                        doTurnOffScreen();
                    } catch (InterruptedException e) {
                        Timber.d("Interrupted thread: Turning off screen cancelled.");
                    }
                }
            }
        };
    }

    private void doTurnOffScreen() {
        turningOffScreen = true; // Issue #68. Avoid interrupting the thread if screen is already being turned off.
        lastTimeScreenOnOrOff = System.currentTimeMillis();
        if (settings.isVibrateWhileLocking()) {
            vibrate();
        }
        Timber.i("Switched from 'far' to 'near'.");
        if (settings.isLockScreenWithPowerButton()) {
            Timber.i("Turning screen off simulating power button press.");
            Root.pressPowerButton();
        } else {
            Timber.i("Turning screen off.");
            try {
                policyManager.lockNow();
            } catch (IllegalStateException e) {
                Timber.e("Failed to run lockNow() to turn off the screen. Probably due to an ongoing call. Exception: " + e);
            } catch (SecurityException e) {
                Timber.e("Failed to run lockNow() to turn off the screen. Probably due to missing device admin rights, which I don't really understand... Exception: " + e);
            }
        }
        turningOffScreen = false;
    }

    public void turnOffScreen() {
        if (waveUpWorldState.isScreenOn()) {
            if (settings.isVibrateWhileLocking()) {
                vibrate();
            }
            turnOffScreenThread = turnOffScreenThread(settings.getSensorCoverTimeBeforeLockingScreen());
            turnOffScreenThread.start();
        }
    }

    public void cancelTurnOff() {
        if (turnOffScreenThread != null && !turningOffScreen) {
            Timber.d("Cancelling turning off of display");
            turnOffScreenThread.interrupt();
            turnOffScreenThread = null;
        }
    }

    public long getLastTimeScreenOnOrOff() {
        return lastTimeScreenOnOrOff;
    }

    public void turnOnScreen() {
        if (!waveUpWorldState.isScreenOn()) {
            lastTimeScreenOnOrOff = System.currentTimeMillis();
            Timber.i("Switched from 'near' to 'far'. Turning screen on");
            if (wakeLock.isHeld()) {
                wakeLock.release();
            }
            wakeLock.acquire(TIME_SCREEN_ON);
        }
    }

    private void vibrate() {
        Vibrator vibrator = (Vibrator) this.context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(50);
    }
}